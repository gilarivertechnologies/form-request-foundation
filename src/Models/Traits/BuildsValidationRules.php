<?php
/**
 * Model trait to be used on models that have validation rules
 */

namespace Gila\LaravelApiHelpers\FormRequestFoundation\Models\Traits;

/**
 * Trait BuildsValidationRules
 * @package Gila\LaravelApiHelpers\FormRequestFoundation\Models\Traits
 */
trait BuildsValidationRules
{
    /**
     * Builds validation rules for a model based on validation type (base, create, update)
     *
     * @param string $validationType
     * @return array
     */
    public function buildValidationRules(string $validationType): array
    {
        $rules = $this->rules();

        if (!array_key_exists($validationType, $rules) || !array_key_exists(static::VALIDATE_BASE, $rules)) {
            return [];
        }

        $baseRules = $rules[static::VALIDATE_BASE];
        $additionalRules = $rules[$validationType];

        $combinedRules = [];

        foreach ($baseRules as $col => $ruleArray) {
            $combinedRules[$col] = $this->buildRuleStringFromArray($ruleArray);
        }

        foreach($additionalRules as $col => $ruleArray) {
            $combinedRules[$col] .= '|' . $this->buildRuleStringFromArray($ruleArray);
        }

        return $combinedRules;
    }

    /**
     * Builds validation messages for a model
     *
     * @return array
     */
    public function buildValidationMessages(): array
    {
        $messages = $this->messages();
        $formatted = [];

        foreach ($messages as $column => $rules) {
            foreach ($rules as $qualifier => $message) {
                $formatted[sprintf('%s.%s', $column, $qualifier)] = sprintf($message, $column);
            }
        }

        return $formatted;
    }

    /**
     * @param array $rules
     * @return string
     */
    private function buildRuleStringFromArray(array $rules)
    {
        $formattedRules = [];
        foreach ($rules as $individualRule) {
            if (is_array($individualRule)) {
                $formattedRules[] = $this->buildRuleStringFromArray($individualRule);
            } else {
                $formattedRules[] = $individualRule;
            }
        }

        return implode('|', $formattedRules);
    }
}
